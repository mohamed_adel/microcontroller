library ieee;
use ieee.std_logic_1164.all;

entity D_latch is
   port(clk, D : in std_logic;
       Q, notQ : out std_logic);
end D_latch;
--
architecture func of D_latch is
   component  nandGate is
      port(A, B : in std_logic;
              F : out std_logic);
   end component;
   signal topWire, botWire, Qback, notQback : std_logic;

begin
   G1: nandGate port map(D, clk, topWire);
   G2: nandGate port map(topWire, clk, botWire);
   G3: nandGate port map(topWire, notQback, Qback);
   G4: nandGate port map(Qback, botWire, notQback);
   Q <= Qback;
   notQ <= notQback;
end func;
