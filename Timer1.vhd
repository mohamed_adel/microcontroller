library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity Timer1 is
	port(
		clk           : in    std_logic;
		rst           : in    std_logic;
		en            : in    std_logic;
		rd_wr         : in    std_logic;
		dataBus       : inout std_logic_vector(7 downto 0);
		addressEnable : in    std_logic;
		regSelect     : in    std_logic_vector(1 downto 0);
		T1IF          : out   std_logic
	);
end entity Timer1;

architecture RTL of Timer1 is
	Component my_nDFF is
		Generic(n : integer := 8);
		port(Clk, Rst, enable : in  std_logic;
			 d                : in  std_logic_vector(n - 1 downto 0);
			 q                : out std_logic_vector(n - 1 downto 0));
	end component;

	Component Ntri is
		Generic(n : integer := 8);
		port(
			ins  : in  std_logic_vector(n - 1 downto 0);
			es   : in  std_logic;
			outs : out std_logic_vector(n - 1 downto 0));
	end component;

	component mux_2x1
		port(a : in  std_logic;
			 b : in  std_logic;
			 s : in  std_logic;
			 c : out std_logic);
	end component mux_2x1;

	component decoder_1x2
		port(x0, e  : in  std_logic;
			 d0, d1 : out std_logic);
	end component decoder_1x2;

	component decoder_2x4
		port(x0, x1, e      : in  std_logic;
			 d0, d1, d2, d3 : out std_logic);
	end component decoder_2x4;

	component Prescaler
		port(clk           : in  std_logic;
			 rst           : in  std_logic;
			 mode          : in  std_logic_vector(1 downto 0);
			 prescaled_clk : out std_logic);
	end component Prescaler;

	component Postscaler
		port(clk  : in  std_logic;
			 rst  : in  std_logic;
			 mode : in  std_logic_vector(1 downto 0);
			 OVF  : in  std_logic;
			 INTF : out std_logic);
	end component Postscaler;

	--! Control Register \n
	--! 0   => ON/OFF \n
	--! 1   => Prescalar Select \n
	--! 2,3 => Prescalar Mode \n
	--! 4,5 => Postscale Moe
	signal control_output    : std_logic_vector(7 downto 0);
	--! Timer/Counter Register
	signal timer_output      : std_logic_vector(7 downto 0);
	signal timer_output_reg  : std_logic_vector(7 downto 0);
	--! Period Register
	signal period_output     : std_logic_vector(7 downto 0);
	signal period_output_reg : std_logic_vector(7 downto 0);
	--! Timer ON/OFF
	signal T1ON              : std_logic;
	--! Prescale ON/OFF
	signal T1PS              : std_logic;
	--! Prescale Mode From Control Register
	signal T1CKPS            : std_logic_vector(1 downto 0);
	--! Postscale Mode
	signal T1OUTPS           : std_logic_vector(1 downto 0);
	--! Prescaled Clock
	signal prescaled_clk     : std_logic;
	--! Final Clock
	signal final_clk         : std_logic;
	--! Internal Overflow Signal
	--		signal OVF               : std_logic;

	signal post_scale_target  : std_logic_vector(2 downto 0);
	signal post_scale_counter : std_logic_vector(2 downto 0);

	signal enable : std_logic;

	signal enable_control_write : std_logic;
	signal enable_timer_write   : std_logic;
	signal enable_period_write  : std_logic;

	signal enable_control_read : std_logic;
	signal enable_timer_read   : std_logic;
	signal enable_period_read  : std_logic;

	signal address_select1, address_select2 : std_logic;
begin
	-- Start Address from x44 to x47 = 0100 0100
	--enable <= en and addressBus(6) and addressBus(2) and (not (addressBus(7) or addressBus(5) or addressBus(4) or addressBus(3)));
	enable          <= en and addressEnable;
	address_select1 <= enable and not regSelect(1);
	address_select2 <= enable and regSelect(1) and not regSelect(0);

	-- bus Read from, bus write to
	--	enable_control_write <= enable and not regSelect(1) and not regSelect(0) and not rd_wr;
	--	enable_timer_write   <= enable and not regSelect(1) and regSelect(0) and not rd_wr;
	--
	--	enable_control_read <= enable and not regSelect(1) and not regSelect(0) and rd_wr;
	--	enable_timer_read   <= enable and not regSelect(1) and regSelect(0) and rd_wr;

	lbl_address_decoder1 : decoder_2x4
		port map(x0 => regSelect(0),
			     x1 => rd_wr,
			     e  => address_select1,
			     d0 => enable_control_write,
			     d1 => enable_timer_write,
			     d2 => enable_control_read,
			     d3 => enable_timer_read);

	lbl_address_decoder2 : decoder_1x2
		port map(x0 => rd_wr,
			     e  => address_select2,
			     d0 => enable_period_write,
			     d1 => enable_period_read);

	--	enable_period_write <= enable and not address_select and not rd_wr;
	--	enable_period_read  <= enable and not address_select and rd_wr;

	-- Identify Counter Options
	T1ON    <= en and control_output(0);
	T1PS    <= control_output(1) and not enable_timer_write and not enable_period_write;
	T1CKPS  <= control_output(3 downto 2);
	T1OUTPS <= control_output(5 downto 4);

	post_scale_target <= "000" when T1OUTPS = "00"
		else "001" when T1OUTPS = "01"
		else "011" when T1OUTPS = "10"
		else "111";

	lbl_prescaler : Prescaler port map(clk, rst, T1CKPS, prescaled_clk);

	lbl_clk_mux : mux_2x1 port map(clk, prescaled_clk, T1PS, final_clk);

	--! Counting Process
	process(final_clk, rst, enable_period_write, enable_timer_write, timer_output_reg)
	begin
		if rst = '1' then
			timer_output       <= X"00";
			period_output      <= X"FF";
			post_scale_counter <= "000";
			T1IF               <= '0';
		elsif enable_period_write = '1' then
			-- Reset timer and postscaler when reading period
			period_output      <= period_output_reg;
			timer_output       <= X"00";
			post_scale_counter <= "000";
		elsif enable_timer_write = '1' then
			-- Reset postscaler when reading timer
			timer_output       <= timer_output_reg;
			post_scale_counter <= "000";
		elsif (rising_edge(final_clk)) then
			--			if enable_period_write = '1' then
			--				-- Reset timer and postscaler when reading period
			----				period_output      <= period_output_reg;
			--				timer_output       <= X"00";
			--				post_scale_counter <= "000";
			--			elsif enable_timer_write = '1' then
			--				-- Reset postscaler when reading timer
			--				timer_output       <= timer_output_reg;
			--				post_scale_counter <= "000";
			if T1ON = '1' then
				-- Compare
				if (timer_output = period_output) then
					-- Postscaler
					if (post_scale_counter = post_scale_target) then
						T1IF               <= '1';
						post_scale_counter <= "000";
					else
						T1IF               <= '0';
						post_scale_counter <= post_scale_counter + 1;
					end if;
					timer_output <= X"00";
				else
					T1IF         <= '0';
					timer_output <= timer_output + '1';
				end if;
			end if;
		end if;
	end process;

	--	lbl_postscaler : Postscaler port map(clk,'0', T1OUTPS, '0', OVF);

	lbl_register_control : my_nDFF generic map(8) port map(clk, rst, enable_control_write, dataBus, control_output);
	lbl_register_timer : my_nDFF generic map(8) port map(clk, rst, enable_timer_write, dataBus, timer_output_reg);
	lbl_resister_period : my_nDFF generic map(8) port map(clk, rst, enable_period_write, dataBus, period_output_reg);

	lbl_tristate_control : Ntri generic map(8) port map(control_output, enable_control_read, dataBus);
	lbl_tristate_timer : Ntri generic map(8) port map(timer_output, enable_timer_read, dataBus);
	lbl_tristate_period : Ntri generic map(8) port map(period_output, enable_period_read, dataBus);

--	process(final_clk, enable_timer_read, T1ON)
--	begin
--		if (rising_edge(final_clk) and T1ON = '1' and enable_timer_read = '1') then
--			if (timer_output = period_output) then
--				OVF          <= '1';
--				timer_output <= X"00";
--			else
--				OVF          <= '0';
--				timer_output <= timer_output + '1';
--			end if;
--		end if;
--	end process;

end architecture RTL;


