library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity Prescaler is
	port(
		clk           : in  std_logic;
		rst           : in  std_logic;
		mode          : in  std_logic_vector(1 downto 0);
		prescaled_clk : out std_logic
	);
end entity Prescaler;

architecture RTL of Prescaler is
	signal p_clk         : std_logic;
	signal counter       : std_logic_vector(2 downto 0);
	signal scale_counter : std_logic_vector(2 downto 0);
begin
	scale_counter <= "000" when mode = "00"
		else "001" when mode = "01"
		else "011" when mode = "10"
		else "111";

	process(clk, rst)
	begin
		if (rst = '1') then
			p_clk   <= '0';
			counter <= "000";
		elsif rising_edge(clk) then
			if counter = scale_counter then
				p_clk   <= not p_clk;
				counter <= "000";
			else
				counter <= counter + 1;
			end if;
		end if;
	end process;
	
	prescaled_clk <= p_clk;
	
end architecture RTL;
