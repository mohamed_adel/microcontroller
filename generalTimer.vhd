library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity generalTimer is
	port(
		CLK    : in  STD_LOGIC;
		Data   : in  STD_LOGIC_VECTOR(7 downto 0);
		OUTPUT : out STD_LOGIC_VECTOR(7 downto 0);
		reset  : in  std_logic
	);
end entity generalTimer;

architecture RTL of generalTimer is
	signal counter : STD_LOGIC_VECTOR(7 downto 0) := (others => '0');
begin
	OUTPUT <= counter;

	count_process : process(CLK)
	begin
		if (reset = '1') then
			counter <= (others => '0');
		elsif rising_edge(CLK) then
			if counter = Data then
				counter <= (others => '0');
			else
				counter <= counter + 1;
			end if;
		end if;
	end process;

end architecture RTL;
