library ieee;
use ieee.std_logic_1164.all;

entity decoder_2x4 is
	port(	
		x0,x1,e : in std_logic;
		d0,d1,d2,d3 : out std_logic
	);
end decoder_2x4;

architecture a_decoder_2x4 of decoder_2x4 is
begin
	d0 <= '1' when e = '1' and x1 = '0' and x0 = '0' else '0';
	d1 <= '1' when e = '1' and x1 = '0' and x0 = '1' else '0';
	d2 <= '1' when e = '1' and x1 = '1' and x0 = '0' else '0';
	d3 <= '1' when e = '1' and x1 = '1' and x0 = '1' else '0';
end a_decoder_2x4;