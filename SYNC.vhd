library ieee;
use ieee.std_logic_1164.all;

entity Sync is
	port(clk      : in  std_logic;
		 ex_clk   : in  std_logic;
		 sync_clk : out std_logic
	);
end Sync;

architecture RTL of Sync is
	component my_DFF
		port(d, clk, rst, enable : in  std_logic;
			 q                   : out std_logic);
	end component my_DFF;

	signal rise, fall : std_logic;
begin
	r1 : my_DFF port map(ex_clk, clk, '0', '1', rise);
	r2 : my_DFF port map(rise, clk, '0', '1', fall);
	sync_clk <= rise and not fall; 
	
end architecture;