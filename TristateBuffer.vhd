library ieee;
Use ieee.std_logic_1164.all;
entity TriStateBuffer is
	port(input, E : in  std_logic;
		 output   : out std_logic
	);
end TriStateBuffer;
Architecture TriStateBuffer of TriStateBuffer is
begin
	output <= input when E = '1'
		else 'Z';
end TriStateBuffer;