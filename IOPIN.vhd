library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity IOPIN is
	-- Direction 1 read from pin to the internal latch , out to pin from internal latch
	port(
		clk, rst, enable                          : in    std_logic;
		Direction, Selector, SpecialOutput, RD_WR : in    std_logic;
		DataBus, IOPin                            : inout std_logic
	);
end entity IOPIN;
architecture RTL of IOPIN is
	component my_DFF is
		port(d, clk, rst, enable : in  std_logic;
			 q                   : out std_logic);
	end component;
	component TriStateBuffer is
		port(input, E : in  std_logic;
			 output   : out std_logic
		);
	end component;
	component mux_2x1 is
		port(
			a : in  std_logic;          --! Input 1
			b : in  std_logic;          --! Input 2
			s : in  std_logic;          --! Selector
			c : out std_logic           --! Mux output
		);
	end component;
	signal dataLatchQ, InputQ, NotRD_WR    : std_logic;
	signal MultiplixerOutput, NotDirection : std_logic;
	signal EnableDataLatch, EnbleInputTri  : std_logic;
begin
	Multiplixer : mux_2x1 port map(dataLatchQ, SpecialOutput, Selector, MultiplixerOutput);
	DataLatch : my_DFF port map(DataBus, clk, rst, EnableDataLatch, dataLatchQ);
	Input : my_DFF port map(IOPin, clk, rst, Direction, InputQ);
	EnableDataLatch <= (NotRD_WR and enable);
	EnbleInputTri <= (RD_WR and enable);
	NotDirection    <= (not Direction);
	NotRD_WR        <= (not RD_WR);
	WriteTriState : TriStateBuffer port map(MultiplixerOutput, NotDirection, IOPin);
	InputTriState : TriStateBuffer port map(InputQ, EnbleInputTri, DataBus);
end architecture RTL;
