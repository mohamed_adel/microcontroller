library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity Postscaler is
	port(
		clk  : in  std_logic;
		rst  : in  std_logic;
		mode : in  std_logic_vector(1 downto 0);
		OVF  : in  std_logic;
		INTF : out std_logic
	);
end entity Postscaler;

architecture RTL of Postscaler is
	--component my_nadder
	--	generic(n : integer := 8);
	--	port(a, b : in  std_logic_vector(n - 1 downto 0);
	--		 cin  : in  std_logic;
	--		 s    : out std_logic_vector(n - 1 downto 0);
	--		 cout : out std_logic);
	--end component my_nadder;

	signal counter       : std_logic_vector(2 downto 0) := "000";
	signal scale_counter : std_logic_vector(2 downto 0);
begin
	scale_counter <= "000" when mode = "00"
		else "001" when mode = "01"
		else "011" when mode = "10"
		else "111";

--		process(OVF)
--		begin
--			if (OVF = '0') then
--				INTF <= '0';
--			elsif (OVF = '1') then
--				if (counter = scale_counter) then
--					INTF          <= '1';
--					counter  <= "000";
--				else
--					INTF         <= '0';
--					counter <= counter + '1';
--				end if;
--			end if;
--		end process;

		process(clk, OVF, rst)
		begin
			if rst = '1' then
				INTF <= '0';
				counter <= "000";
			elsif (OVF = '0') then
				INTF <= '0';
			elsif rising_edge(clk) then
				if counter = scale_counter then
					INTF    <= '1';
					counter <= "000";
				else
					INTF         <= '0';
					counter <= counter + 1;
				end if;
			end if;
		end process;

end architecture RTL;
