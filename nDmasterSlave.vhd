library ieee;
Use ieee.std_logic_1164.all;
Entity nMasterSlave is
Generic ( n : integer := 16);
port( Clk : in std_logic;
Rest : in std_logic;
D : in std_logic_vector(n-1 downto 0);
Q,notQ : out std_logic_vector(n-1 downto 0));
end nMasterSlave;

Architecture nMasterSlave of nMasterSlave is
Component ms_Data_ff is
   port( clk, D,Rest  : in std_logic;
        Q, notQ  : out std_logic);
end component;
begin
loop1: for i in 0 to n-1 generate
fx: ms_Data_ff port map(Clk,D(i),Rest,Q(i),notQ(i));
end generate;
end nMasterSlave;
