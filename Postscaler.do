vsim work.Postscaler
add wave -position end  sim:/postscaler/mode
add wave -position end  sim:/postscaler/OVF
add wave -position end  sim:/postscaler/INTF
add wave -position end  sim:/postscaler/counter
add wave -position end  sim:/postscaler/scale_counter

force -freeze sim:/postscaler/OVF 0 0, 1 {50 ps} -r 100
force -freeze sim:/postscaler/mode 00 0
run
run
run
force -freeze sim:/postscaler/mode 01 0
run
run
run
run
run
run
force -freeze sim:/postscaler/mode 10 0
run
run
run
run
run
run
run
run
force -freeze sim:/postscaler/mode 11 0
run
run
run
run
run
run
run
run
run
run
run
run
run
run
run
run
run
