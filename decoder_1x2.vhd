library ieee;
use ieee.std_logic_1164.all;

entity decoder_1x2 is
	port(	
		x0,e : in std_logic;
		d0,d1 : out std_logic
	);
end decoder_1x2;

architecture a_decoder_1x2 of decoder_1x2 is
begin
	d0 <= '1' when e = '1' and x0 = '0' else '0';
	d1 <= '1' when e = '1' and x0 = '1' else '0';
end a_decoder_1x2;