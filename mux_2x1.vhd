library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--! Multiplexer 2x1
entity mux_2x1 is
	port (
		a : in std_logic;	--! Input 1
		b : in std_logic;	--! Input 2
		s : in std_logic;	--! Selector
		c : out std_logic	--! Mux output
	);
end entity mux_2x1;

architecture RTL of mux_2x1 is

begin
	c <= a when s = '0'
	else b;

end architecture RTL;
