vsim work.timer1
#restart -f -nowave

echo "####################"
echo "##  Adding Waves  ##"	
echo "####################"

echo "Global Signals"
add wave -position end  sim:/timer1/rst
add wave -position end  sim:/timer1/en
add wave -position end  sim:/timer1/rd_wr

echo "Clocks"
add wave -position end  sim:/timer1/clk
add wave -position end  sim:/timer1/prescaled_clk
add wave -position end  sim:/timer1/final_clk

echo "Busses"
add wave -position end  sim:/timer1/dataBus
add wave -position end  sim:/timer1/addressEnable
add wave -position end  sim:/timer1/regSelect

echo "Registers - Outputs"
add wave -position end  sim:/timer1/control_output
add wave -position end  sim:/timer1/period_output
add wave -position end  sim:/timer1/period_output_reg
add wave -position end  sim:/timer1/timer_output
add wave -position end  sim:/timer1/timer_output_reg
add wave -position end  sim:/timer1/T1IF

echo "Options Configurations (selector)"
add wave -position end  sim:/timer1/T1CKPS
add wave -position end  sim:/timer1/T1ON
add wave -position end  sim:/timer1/T1OUTPS
add wave -position end  sim:/timer1/T1PS

echo "Enable and Disable"
add wave -position end  sim:/timer1/enable
add wave -position end  sim:/timer1/enable_control_write
add wave -position end  sim:/timer1/enable_period_write
add wave -position end  sim:/timer1/enable_timer_write
add wave -position end  sim:/timer1/enable_control_read
add wave -position end  sim:/timer1/enable_period_read
add wave -position end  sim:/timer1/enable_timer_read

# postscaler
add wave -position end  sim:/timer1/post_scale_counter
add wave -position end  sim:/timer1/post_scale_target
add wave -position 14  sim:/timer1/lbl_prescaler/counter

echo "\nReset Timer, Time: $now"
force -freeze sim:/timer1/clk 0 0, 1 {50 ps} -r 100
force -freeze sim:/timer1/rd_wr 1 0
force -freeze sim:/timer1/rst 1 0
force -freeze sim:/timer1/en 1 0
run

echo "##############################################"
echo "##  Load Data & Configuration from Databus  ##"
echo "##############################################"

force -freeze sim:/timer1/rst 0 0
echo "Wrting to Registers"
force -freeze sim:/timer1/rd_wr 0 0

echo "\tWriting to Control Register, Time: $now"
force -freeze sim:/timer1/regSelect 00 0
force -freeze sim:/timer1/addressEnable 1 0
force -freeze sim:/timer1/dataBus 00000001 0
run
echo "\t\tNoForce, Time: $now"
noforce sim:/timer1/dataBus
noforce sim:/timer1/addressEnable
run

echo "\tWriting to Timer Register, Time: $now"
force -freeze sim:/timer1/regSelect 01 0
force -freeze sim:/timer1/addressEnable 1 0
force -freeze sim:/timer1/dataBus 00000001 0
run
echo "\t\tNoForce, Time: $now"
noforce sim:/timer1/dataBus
noforce sim:/timer1/addressEnable
run


echo "\tWriting to Period Register, Time: $now"
force -freeze sim:/timer1/regSelect 10 0
force -freeze sim:/timer1/addressEnable 1 0
force -freeze sim:/timer1/dataBus 00000001 0
run
echo "\t\tNoForce, Time: $now"
noforce sim:/timer1/dataBus
noforce sim:/timer1/addressEnable
run


echo "Reading from Registers"
force -freeze sim:/timer1/rd_wr 1 0

echo "\tReading from Control Register, Time: $now"
force -freeze sim:/timer1/regSelect 00 0
force -freeze sim:/timer1/addressEnable 1 0
run
echo "\t\tNoForce, Time: $now"
noforce sim:/timer1/addressEnable
run

echo "\tReading from Timer Register, Time: $now"
force -freeze sim:/timer1/regSelect 01 0
force -freeze sim:/timer1/addressEnable 1 0
run
echo "\t\tNoForce, Time: $now"
noforce sim:/timer1/addressEnable
run

echo "\tReading from Period Register, Time: $now"
force -freeze sim:/timer1/regSelect 10 0
force -freeze sim:/timer1/addressEnable 1 0
run
echo "\t\tNoForce, Time: $now"
noforce sim:/timer1/addressEnable
run


#echo "###########################################"
#echo "##  Works on System Internal Clock ​only  ##"
#echo "###########################################"

#echo "##########################"
#echo "##  8-bit Timer Period  ##"
#echo "##########################"

echo "##############################################"
echo "##  4 mode Prescaler (1:2, 1:4, 1:8, 1:16)  ##"
echo "##############################################"

echo "Configure Control Register 1:2, Time: $now"
force -freeze sim:/timer1/rd_wr 0 0
force -freeze sim:/timer1/regSelect 00 0
force -freeze sim:/timer1/addressEnable 1 0
force -freeze sim:/timer1/dataBus 00000011 0
run
echo "\t\tNoForce, Time: $now"
noforce sim:/timer1/dataBus
noforce sim:/timer1/addressEnable
run
run 400 ps

echo "Configure Control Register 1:4, Time: $now"
force -freeze sim:/timer1/rd_wr 0 0
force -freeze sim:/timer1/regSelect 00 0
force -freeze sim:/timer1/addressEnable 1 0
force -freeze sim:/timer1/dataBus 00000111 0
run
echo "\t\tNoForce, Time: $now"
noforce sim:/timer1/dataBus
noforce sim:/timer1/addressEnable
run
run 800 ps


echo "##############################################"
echo "##  4-mode Postscaler (1:1, 1:2, 1:4, 1:8)  ##"
echo "##############################################"

echo "\tWriting to Period Register, Time: $now"
force -freeze sim:/timer1/regSelect 10 0
force -freeze sim:/timer1/addressEnable 1 0
force -freeze sim:/timer1/dataBus 00000011 0
run
echo "\t\tNoForce, Time: $now"
noforce sim:/timer1/dataBus
noforce sim:/timer1/addressEnable
run

echo "\tConfigure CR Postscale 1:1, Time: $now"
force -freeze sim:/timer1/rd_wr 0 0
force -freeze sim:/timer1/regSelect 00 0
force -freeze sim:/timer1/addressEnable 1 0
force -freeze sim:/timer1/dataBus 00000001 0
run
echo "\t\tNoForce, Time: $now"
noforce sim:/timer1/dataBus
noforce sim:/timer1/addressEnable
run
run 400 ps

echo "Configure CR Postscale 1:2, Time: $now"
force -freeze sim:/timer1/rd_wr 0 0
force -freeze sim:/timer1/regSelect 00 0
force -freeze sim:/timer1/addressEnable 1 0
force -freeze sim:/timer1/dataBus 00010001 0
run
echo "\t\tNoForce, Time: $now"
noforce sim:/timer1/dataBus
noforce sim:/timer1/addressEnable
run
run 800 ps

echo "Configure CR Postscale 1:4, Time: $now"
force -freeze sim:/timer1/rd_wr 0 0
force -freeze sim:/timer1/regSelect 00 0
force -freeze sim:/timer1/addressEnable 1 0
force -freeze sim:/timer1/dataBus 00100001 0
run
echo "\t\tNoForce, Time: $now"
noforce sim:/timer1/dataBus
noforce sim:/timer1/addressEnable
run
run 800 ps


#echo "##########################"
#echo "##  Interrupt overflow  ##"
#echo "##########################"