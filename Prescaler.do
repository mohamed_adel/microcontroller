vsim work.prescaler

add wave -position end  sim:/prescaler/rst
add wave -position end  sim:/prescaler/clk
add wave -position end  sim:/prescaler/mode
add wave -position end  sim:/prescaler/prescaled_clk
add wave -position end  sim:/prescaler/p_clk
add wave -position end  sim:/prescaler/scale_counter
add wave -position end  sim:/prescaler/counter


force -freeze sim:/prescaler/clk 0 0, 1 {50 ps} -r 100
force -freeze sim:/prescaler/mode 00 0
force -freeze sim:/prescaler/rst 1 0
run
force -freeze sim:/prescaler/rst 0 0
run
run
run
run
run
force -freeze sim:/prescaler/mode 01 0
run
run
run
run
run
run
run
run
run
run
run
run
run
run
force -freeze sim:/prescaler/mode 10 0
run
run
run
run
run
run
run
run
run
run
run
run
run
force -freeze sim:/prescaler/mode 11 0
run
run
run
run
run
run
run
run
run
run
run
run
run
run
run
run
run
run
run
run
run
run
run
run
run
run
run
run
run
run
run
run
run
run
run
run
run
run
run
