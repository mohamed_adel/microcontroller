vsim work.sync

add wave -position end  sim:/sync/clk
add wave -position end  sim:/sync/ex_clk
add wave -position end  sim:/sync/sync_clk
force -freeze sim:/sync/clk 0 0, 1 {25 ps} -r 50
force -freeze sim:/sync/ex_clk 0 0, 1 {40 ps} -r 80
run
