library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
---------------------------------------------------------------- 
----------------------------------------------------------------
entity InterruptEnable is
	port(
		clk        : in    std_logic;
		rst        : in    std_logic;
		dataBus    : inout std_logic_vector(7 downto 0);
		address, S1, S0     : in    std_logic;
		rd_wr      : in    std_logic;
		en         : in    std_logic;
		output     : out   std_logic_vector(7 downto 0)
	);
end entity InterruptEnable;

architecture RTL of InterruptEnable is
	Component my_nDFF is
		Generic(n : integer := 8);
		port(Clk, Rst, enable : in  std_logic;
			 d                : in  std_logic_vector(n - 1 downto 0);
			 q                : out std_logic_vector(n - 1 downto 0));
	end component;
	Component Ntri is
		Generic(n : integer := 8);
		port(
			ins  : in  std_logic_vector(n - 1 downto 0);
			es   : in  std_logic;
			outs : out std_logic_vector(n - 1 downto 0));
	end component;
	-- in this section we define all used signals 
	signal Enable                                : std_logic; -- start address is 0x48 and we will take 8 addresses for loading registers and future improvements
	signal Enable_Latch, Latch_Read, Latch_Write : std_logic;
	signal Latch_Output                          : std_logic_vector(7 downto 0);
begin
	-----Register read,write signals
	Latch_Read   <= Enable_Latch and rd_wr and en;
	Latch_Write  <= Enable_Latch and (not rd_wr) and en;
	--------------------------------
	Enable       <= address;
	Enable_Latch <= (Enable  and (not S1) and (not S0));
	output       <= Latch_Output;
	-- ComponentS Definitions
	----Registers
	lbl_register_Latch : my_nDFF generic map(8) port map(clk, rst, Latch_Write, dataBus, Latch_Output);
	----TRistates
	lbl_tristate_ICR : Ntri generic map(8) port map(Latch_Output, Latch_Read, dataBus);
end architecture RTL; 
