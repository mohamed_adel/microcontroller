library ieee;
use ieee.std_logic_1164.all;

entity ms_Data_ff is
	port(clk, D  : in  std_logic;
		 Rest    : in  std_logic;
		 Q, notQ : out std_logic);
end ms_Data_ff;
--
architecture func of ms_Data_ff is
	--import the inverter entity as a component
	component notGate is
		port(inPort  : in  std_logic;
			 outPort : out std_logic);
	end component;

	--import the D-latch entity as a component
	component D_latch is
		port(clk, D  : in  std_logic;
			 Q, notQ : out std_logic);
	end component;

	--interconnecting wires carrying signals to the components
	--we define a dummy signal because all ports must be wired
	signal invOut1, invOut2, Dout, dummy : std_logic := '0';
	signal tempQ, tempnotQ               : std_logic;
begin
	G1 : notGate port map(clk, invOut1);
	G2 : notGate port map(invOut1, invOut2);
	G3 : D_latch port map(invOut1, D, Dout, dummy);
	G4 : D_latch port map(invOut2, Dout, tempQ, tempnotQ);
	Q <= tempQ when Rest = '0'
		else '0' when Rest = '1';
	notQ <= tempnotQ when Rest = '0'
		else '0' when Rest = '1';
end func;