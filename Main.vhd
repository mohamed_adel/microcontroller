library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
----
---- @ interrupt enable bit 0 for timer 0 , 1 for timer 1 
entity Main is
	port(
		clk              : in    std_logic;
		rst              : in    std_logic;
		en               : in    std_logic;
		ext_clk          : in    std_logic;
		rd_wr            : in    std_logic;
		GeneralInterrupt : out   std_logic;
		addressBus       : in    std_logic_vector(7 downto 0);
		dataBus          : inout std_logic_vector(7 downto 0);
		IOpins           : inout std_logic_vector(7 downto 0)
	);
end entity Main;

architecture RTL of Main is
	signal Timer0Interrupt, Timer0Output                       : std_logic;
	signal Timer1Interrupt, Timer1Output                       : std_logic;
	signal PWM_Output                                          : std_logic;
	signal interruptEnableOutput                               : std_logic_vector(7 downto 0);
	signal PWM_address, GBIO_address, Enable_Interrupt_address : std_logic;
	signal Timer0_address, Timer1_address                      : std_logic;
	Component Timer0 is
		port(
			clk           : in    std_logic; --! System/Internal Clock
			rst           : in    std_logic; --! Global Reset
			en            : in    std_logic; --! Global Enable
			rd_wr         : in    std_logic; --! Global Read/Write
			ext_clk       : in    std_logic; --! External Clock
			dataBus       : inout std_logic_vector(7 downto 0); --! Main Data Bus
			addressEnable : in    std_logic;
			regSelect     : in    std_logic_vector(1 downto 0);
			T0IF          : out   std_logic --! Timer0 Overflow Interrupt (Timer Ouptut)
		);
	end component;
	Component PWM is
		port(
			clk     : in    std_logic;
			rst     : in    std_logic;
			dataBus : inout std_logic_vector(7 downto 0);
			rd_wr   : in    std_logic;
			en      : in    std_logic;
			Address : in    std_logic;
			S1, S0  : in    std_logic;
			output  : out   std_logic
		);
	end Component;
	Component InterruptEnable is
		port(
			clk             : in    std_logic;
			rst             : in    std_logic;
			dataBus         : inout std_logic_vector(7 downto 0);
			address, S1, S0 : in    std_logic;
			rd_wr           : in    std_logic;
			en              : in    std_logic;
			output          : out   std_logic_vector(7 downto 0)
		);
	end Component;
	Component GBIO is
		port(
			clk                 : in    std_logic;
			rst                 : in    std_logic;
			dataBus             : inout std_logic_vector(7 downto 0);
			address, S1, S0     : in    std_logic;
			rd_wr               : in    std_logic;
			en                  : in    std_logic;
			Timer0, Timer1, PWM : in    std_logic;
			output              : inout std_logic_vector(7 downto 0)
		);
	end Component GBIO;
	Component Timer1 is
		port(
			clk           : in    std_logic;
			rst           : in    std_logic;
			en            : in    std_logic;
			rd_wr         : in    std_logic;
			dataBus       : inout std_logic_vector(7 downto 0);
			addressEnable : in    std_logic;
			regSelect     : in    std_logic_vector(1 downto 0);
			T1IF          : out   std_logic
		);
	end Component Timer1;

begin
	----- Addressing signals ------ 
	PWM_address              <= (en and (not addressBus(7)) and (addressBus(6)) and (not addressBus(5)) and (not addressBus(4)) and (addressBus(3)) and (not addressBus(2)));
	GBIO_address             <= (en and (not addressBus(7)) and (addressBus(6)) and (addressBus(5)) and (not addressBus(4)) and (not addressBus(3)) and (addressBus(2)));
	Enable_Interrupt_address <= (en and (not addressBus(7)) and (not addressBus(6)) and (addressBus(5)) and (addressBus(4)) and (not addressBus(3)) and (not addressBus(2)));
	Timer0_address           <= (en and (not addressBus(7)) and (addressBus(6)) and (not addressBus(5)) and (not addressBus(4)) and (not addressBus(3)) and (not addressBus(2)));
	Timer1_address           <= (en and (not addressBus(7)) and (addressBus(6)) and (not addressBus(5)) and (not addressBus(4)) and (not addressBus(3)) and (addressBus(2)));

	----- end Addressing signals ------
	lbl_Timer0 : Timer0 port map(clk, rst, en, rd_wr, ext_clk, dataBus, Timer0_address, addressBus(1 downto 0), Timer0Output);
	lbl_Timer1 : Timer1 port map(clk, rst, en, rd_wr, dataBus, Timer1_address, addressBus(1 downto 0), Timer1Output);
	lbl_PWM : PWM port map(clk, rst, dataBus, rd_wr, en, PWM_address, addressBus(1), addressBus(0), PWM_Output);
	lbl_InterruptEnable : InterruptEnable port map(clk, rst, dataBus, Enable_Interrupt_address, addressBus(1), addressBus(0), rd_wr, en, interruptEnableOutput);
	lbl_GBIO : GBIO port map(clk, rst, dataBus, GBIO_address, addressBus(1), addressBus(0), rd_wr, en, Timer0Interrupt, Timer1Interrupt, PWM_Output, IOpins);

	Timer0Interrupt  <= Timer0Output and interruptEnableOutput(0);
	Timer1Interrupt  <= Timer1Output and interruptEnableOutput(1);
	GeneralInterrupt <= Timer0Interrupt or Timer1Interrupt;
end architecture RTL;
