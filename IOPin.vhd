library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity IOPin is
	port(
		clk   : in    std_logic;
		rst   : in    std_logic;
		en    : in    std_logic;
		rd_wr : in    std_logic;
		X, Y  : inout std_logic
	);
end entity IOPin;

architecture RTL of IOPin is
	component my_DFF is
		port(d, clk, rst, enable : in  std_logic;
			 q                   : out std_logic);
	end component;
	component TriStateBuffer is
		port(input, E : in  std_logic;
			 output   : out std_logic
		);
	end component;
	signal input, output       : std_logic;
	signal Not_rd_wr, enX, enY : std_logic;
begin
	enY <= en and rd_wr;
	enX <= en and (not rd_Wr);
	lbl_my_DFF : my_DFF port map(input, clk, rst, en, output);
	lbl_tri_x : TriStateBuffer port map(output, enX, X);
	lbl_tri_y : TriStateBuffer port map(output, enY, Y);
	Not_rd_wr <= not rd_wr;
	input     <= x when enY = '1'
		else y when enX = '1'
		else '0';

end architecture RTL;
