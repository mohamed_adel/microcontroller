library ieee;
Use ieee.std_logic_1164.all;
Entity Ntri is
	Generic(n : integer := 16);
	port(
		ins  : in  std_logic_vector(n - 1 downto 0);
		es   : in  std_logic;
		outs : out std_logic_vector(n - 1 downto 0));
end Ntri;

Architecture b_Ntri of Ntri is
	Component TriStateBuffer is
		port(input, E : in  std_logic;
			 output   : out std_logic
		);
	end component;
begin
	loop1 : for i in 0 to n - 1 generate
		fx : TriStateBuffer port map(ins(i), es, outs(i));
	end generate;
end b_Ntri;