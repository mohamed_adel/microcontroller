library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
----------------------------------------------------------------

----------------------------------------------------------------
entity GBIO is
	port(
		clk                 : in    std_logic;
		rst                 : in    std_logic;
		dataBus             : inout std_logic_vector(7 downto 0);
		address, S0, S1     : in    std_logic;
		rd_wr               : in    std_logic;
		en                  : in    std_logic;
		Timer0, Timer1, PWM : in    std_logic;
		output              : inout std_logic_vector(7 downto 0)
	);
end entity GBIO;

architecture RTL of GBIO is
	Component IOPIN is
		-- Direction 1 read from pin to the internal latch , out to pin from internal latch
		port(
			clk, rst, enable                          : in    std_logic;
			Direction, Selector, SpecialOutput, RD_WR : in    std_logic;
			DataBus, IOPin                            : inout std_logic
		);
	end Component IOPIN;
	Component my_nDFF is
		Generic(n : integer := 8);
		port(Clk, Rst, enable : in  std_logic;
			 d                : in  std_logic_vector(n - 1 downto 0);
			 q                : out std_logic_vector(n - 1 downto 0));
	end component;
	Component Ntri is
		Generic(n : integer := 8);
		port(
			ins  : in  std_logic_vector(n - 1 downto 0);
			es   : in  std_logic;
			outs : out std_logic_vector(n - 1 downto 0));
	end component;
	-- in this section we define all used signals 
	signal Enable, Enable_Data                                                : std_logic; -- start address is 0x48 and we will take 8 addresses for loading registers and future improvements
	signal Enable_Selector, Enable_Direction, Direction_Read, Direction_Write : std_logic;
	signal Selector_Read, Selector_Write                                      : std_logic;
	signal Direction_Output, Selector_Output                                  : std_logic_vector(7 downto 0);
begin
	-----Register read,write signals
	Direction_Read   <= Enable_Direction and rd_wr and en;
	Direction_Write  <= Enable_Direction and (not rd_wr) and en;
	Selector_Read    <= Enable_Selector and rd_wr and en;
	Selector_Write   <= Enable_Selector and (not rd_wr) and en;
	--------------------------------
	Enable           <= address;
	Enable_Data      <= (Enable and (not S1) and (not S0));
	Enable_Direction <= (Enable and (not S1) and (S0));
	Enable_Selector  <= (Enable and (S1) and (not S0));

	-- ComponentS Definitions
	----IOPINS
	GBIO_0 : IOPIN port map(clk, rst, enable, Direction_Output(0), Selector_Output(0), Timer0, RD_WR, DataBus(0), output(0));
	GBIO_1 : IOPIN port map(clk, rst, enable, Direction_Output(1), Selector_Output(1), Timer1, RD_WR, DataBus(1), output(1));
	GBIO_2 : IOPIN port map(clk, rst, enable, Direction_Output(2), Selector_Output(2), PWM, RD_WR, DataBus(2), output(2));
	GBIO_3 : IOPIN port map(clk, rst, enable, Direction_Output(3), '0', '0', RD_WR, DataBus(3), output(3));
	GBIO_4 : IOPIN port map(clk, rst, enable, Direction_Output(4), '0', '0', RD_WR, DataBus(4), output(4));
	GBIO_5 : IOPIN port map(clk, rst, enable, Direction_Output(5), '0', '0', RD_WR, DataBus(5), output(5));
	GBIO_6 : IOPIN port map(clk, rst, enable, Direction_Output(6), '0', '0', RD_WR, DataBus(6), output(6));
	GBIO_7 : IOPIN port map(clk, rst, enable, Direction_Output(7), '0', '0', RD_WR, DataBus(7), output(7));
	----Registers
	lbl_register_Direction : my_nDFF generic map(8) port map(clk, rst, Direction_Write, dataBus, Direction_Output);
	lbl_register_Selector : my_nDFF generic map(8) port map(clk, rst, Selector_Write, dataBus, Selector_Output);
	----TRistates
	lbl_tristate_Direction : Ntri generic map(8) port map(Direction_Output, Direction_Read, dataBus);
	lbl_tristate_Selector : Ntri generic map(8) port map(Selector_Output, Selector_Read, dataBus);

end architecture RTL; 
