vsim work.timer0
#restart -f -nowave

echo "####################"
echo "##  Adding Waves  ##"	
echo "####################"

echo "Global Signals"
add wave -position end  sim:/timer0/rst
add wave -position end  sim:/timer0/en
add wave -position end  sim:/timer0/rd_wr

echo "Clocks"
add wave -position end  sim:/timer0/clk
add wave -position end  sim:/timer0/ext_clk
add wave -position end  sim:/timer0/clk_select
add wave -position end  sim:/timer0/ext_clk_edge_sel
add wave -position end  sim:/timer0/sync_clk
add wave -position end  sim:/timer0/final_clk

echo "Busses"
add wave -position end  sim:/timer0/dataBus
add wave -position end  sim:/timer0/addressEnable
add wave -position end  sim:/timer0/regSelect

echo "Registers - Outputs"
add wave -position end  sim:/timer0/control_output
add wave -position end  sim:/timer0/timer_output
add wave -position end  sim:/timer0/timer_output_reg
add wave -position end  sim:/timer0/T0IF

echo "Options Configurations (selector)"
add wave -position end  sim:/timer0/T0CS
add wave -position end  sim:/timer0/T0ON
add wave -position end  sim:/timer0/T0SE
add wave -position end  sim:/timer0/T0SYNC

echo "Enable and Disable"
add wave -position end  sim:/timer0/enable
add wave -position end  sim:/timer0/enable_control_read
add wave -position end  sim:/timer0/enable_control_write
add wave -position end  sim:/timer0/enable_timer_read
add wave -position end  sim:/timer0/enable_timer_write


echo "\nReset Timer, Time: $now"
force -freeze sim:/timer0/clk 0 0, 1 {50 ps} -r 100
force -freeze sim:/timer0/ext_clk 0 0, 1 {70 ps} -r 140
force -freeze sim:/timer0/rd_wr 1 0
force -freeze sim:/timer0/rst 1 0
force -freeze sim:/timer0/en 1 0
run

echo "##########################################"
echo "##  Reading and Writing Configurations  ##"
echo "##########################################"

force -freeze sim:/timer0/rst 0 0

echo "Writing to Registers"
force -freeze sim:/timer0/rd_wr 0 0
echo "\tWrite to Control Register, Time: $now"
force -freeze sim:/timer0/regSelect 00 0
force -freeze sim:/timer0/addressEnable 1 0
force -freeze sim:/timer0/dataBus 16'h01 0
run
echo "\t\tNoForce, Time: $now"
noforce sim:/timer0/addressEnable
noforce sim:/timer0/dataBus
run

echo "\tWrite to Timer Register, Time: $now"
force -freeze sim:/timer0/regSelect 01 0
force -freeze sim:/timer0/addressEnable 1 0
force -freeze sim:/timer0/dataBus 16'h02 0
run
echo "\t\tNoForce, Time: $now"
noforce sim:/timer0/addressEnable
noforce sim:/timer0/dataBus
run

echo "Reading from Registers"
force -freeze sim:/timer0/rd_wr 1 0
echo "\tRead from Control Register, Time: $now"
force -freeze sim:/timer0/regSelect 00 0
force -freeze sim:/timer0/addressEnable 1 0
run
echo "\t\tNoForce, Time: $now"
noforce sim:/timer0/addressEnable
run

echo "\tRead from Timer Register, Time: $now"
force -freeze sim:/timer0/regSelect 01 0
force -freeze sim:/timer0/addressEnable 1 0
run
echo "\t\tNoForce, Time: $now"
noforce sim:/timer0/addressEnable
run

echo "######################################################"
echo "##  Select System Internal Clock or External Clock  ##"
echo "######################################################"

echo "Configure Timer - Select Internal Clock, Time: $now"
force -freeze sim:/timer0/rd_wr 0 0
force -freeze sim:/timer0/regSelect 00 0
force -freeze sim:/timer0/addressEnable 1 0
force -freeze sim:/timer0/dataBus 00000001 0
run
echo "\tNoForce, Time: $now"
noforce sim:/timer0/addressEnable
noforce sim:/timer0/dataBus
run
echo "\tRun one more time, Time: $now"
run

echo "Configure Timer - Select External Clock, Time: $now"
force -freeze sim:/timer0/rd_wr 0 0
force -freeze sim:/timer0/regSelect 00 0
force -freeze sim:/timer0/addressEnable 1 0
force -freeze sim:/timer0/dataBus 00000011 0
run
echo "\tNoForce, Time: $now"
noforce sim:/timer0/addressEnable
noforce sim:/timer0/dataBus
run
echo "\tRun one more time, Time: $now"
run

echo "#####################################"
echo "##  Select Edge of External Clock  ##"
echo "#####################################"

echo "Configure Timer - Select Rising Edge External Clock, Time: $now"
force -freeze sim:/timer0/rd_wr 0 0
force -freeze sim:/timer0/regSelect 00 0
force -freeze sim:/timer0/addressEnable 1 0
force -freeze sim:/timer0/dataBus 00000011 0
run
echo "\tNoForce, Time: $now"
noforce sim:/timer0/addressEnable
noforce sim:/timer0/dataBus
run
echo "\tRun one more time, Time: $now"
run

echo "Configure Timer - Select Falling Edge External Clock, Time: $now"
force -freeze sim:/timer0/rd_wr 0 0
force -freeze sim:/timer0/regSelect 00 0
force -freeze sim:/timer0/addressEnable 1 0
force -freeze sim:/timer0/dataBus 00000111 0
run
echo "\tNoForce, Time: $now"
noforce sim:/timer0/addressEnable
noforce sim:/timer0/dataBus
run
echo "\tRun one more time, Time: $now"
run

echo "###########################################################"
echo "##  Select Synchronized or Asynchronized External Clock  ##"
echo "###########################################################"

echo "Configure Timer - Select Async. Rising Edge External Clock, Time: $now"
force -freeze sim:/timer0/rd_wr 0 0
force -freeze sim:/timer0/regSelect 00 0
force -freeze sim:/timer0/addressEnable 1 0
force -freeze sim:/timer0/dataBus 00000011 0
run
echo "\tNoForce, Time: $now"
noforce sim:/timer0/addressEnable
noforce sim:/timer0/dataBus
run
echo "\tRun one more time, Time: $now"
run

echo "Configure Timer - Select Sync. External Clock, Time: $now"
force -freeze sim:/timer0/rd_wr 0 0
force -freeze sim:/timer0/regSelect 00 0
force -freeze sim:/timer0/addressEnable 1 0
force -freeze sim:/timer0/dataBus 00001011 0
run
echo "\tNoForce, Time: $now"
noforce sim:/timer0/addressEnable
noforce sim:/timer0/dataBus
run
echo "\tRun one more time, Time: $now"
run

echo "##########################"
echo "##  Interrupt overflow  ##"
echo "##########################"


echo "\tWrite to Timer Register xFD, Time: $now"
force -freeze sim:/timer0/rd_wr 0 0
force -freeze sim:/timer0/regSelect 01 0
force -freeze sim:/timer0/addressEnable 1 0
force -freeze sim:/timer0/dataBus 16'hFD 0
run
echo "\t\tNoForce, Time: $now"
noforce sim:/timer0/addressEnable
noforce sim:/timer0/dataBus
run
echo "Watch T0IF (Interrupt Overflow Signal), Time: $now"
run


