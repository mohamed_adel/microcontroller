--! IEEE Library
library ieee;
--! Standard Logic Package
use ieee.std_logic_1164.all;
--! Standard Logic Unsinged Package
use IEEE.STD_LOGIC_UNSIGNED.ALL;

--! Timer0 	\n
--! ➢ Works on Addresses starts from 0x40 \n
--! ➢ Load Data & Configuration from Databus	\n
--! ➢ Select System Internal Clock or External Clock	\n
--! ➢ Select Synchronized or Asynchronized External Clock	\n
--! ➢ Select Edge of External Clock	\n
--! ➢ Interrupt overflow	\n
entity Timer0 is
	port(
		clk           : in    std_logic; --! System/Internal Clock
		rst           : in    std_logic; --! Global Reset
		en            : in    std_logic; --! Global Enable
		rd_wr         : in    std_logic; --! Global Read/Write
		ext_clk       : in    std_logic; --! External Clock
		dataBus       : inout std_logic_vector(7 downto 0); --! Main Data Bus
		addressEnable : in    std_logic;
		regSelect     : in    std_logic_vector(1 downto 0);
		T0IF          : out   std_logic --! Timer0 Overflow Interrupt (Timer Ouptut)
	);
end entity Timer0;

architecture RTL of Timer0 is
	Component my_nDFF is
		Generic(n : integer := 8);
		port(Clk, Rst, enable : in  std_logic;
			 d                : in  std_logic_vector(n - 1 downto 0);
			 q                : out std_logic_vector(n - 1 downto 0));
	end component;

	Component Ntri is
		Generic(n : integer := 8);
		port(
			ins  : in  std_logic_vector(n - 1 downto 0);
			es   : in  std_logic;
			outs : out std_logic_vector(n - 1 downto 0));
	end component;

	component mux_2x1
		port(a : in  std_logic;
			 b : in  std_logic;
			 s : in  std_logic;
			 c : out std_logic);
	end component mux_2x1;

	component decoder_2x4
		port(x0, x1, e      : in  std_logic;
			 d0, d1, d2, d3 : out std_logic);
	end component decoder_2x4;

	component Sync
		port(clk      : in  std_logic;
			 ex_clk   : in  std_logic;
			 sync_clk : out std_logic);
	end component Sync;

	Component TriStateBuffer is
		port(input, E : in  std_logic;
			 output   : out std_logic
		);
	end component;
	--! Control Register \n
	--! 0-bit Enable \n
	--!	1-bit Clock Select \n
	--! 2-bit External Clock Edge Select \n
	--! 3-bit External Clock Sync./Async. Select (External Clock must be selected)
	signal control_output       : std_logic_vector(7 downto 0);
	--! Timer/Counter Register
	signal timer_output         : std_logic_vector(7 downto 0);
	signal timer_output_reg     : std_logic_vector(7 downto 0);
	--! Enable Timer
	signal T0ON                 : std_logic;
	--! Clock Selector: 0 internal Clk, 1 external Clk
	signal T0CS                 : std_logic;
	--! Selection from previous
	signal clk_select           : std_logic;
	--! External Clock Edge Selector: 0 low-to-high , 1 high-to-low
	signal T0SE                 : std_logic;
	--! Selection from previous
	signal ext_clk_edge_sel     : std_logic;
	--! External Clock Sync. Selector (When External Clock is Selected Only): 0 Sync, 1 Async(Do not Sync)
	signal T0SYNC               : std_logic;
	--! Sync. Clock
	signal sync_clk             : std_logic;
	--! final Clock
	signal final_clk            : std_logic;
	--! When address = x40 ~ x43
	signal enable               : std_logic;
	--! When address = x40
	signal enable_control_write : std_logic;
	--! When address = x41
	signal enable_timer_write   : std_logic;
	--! When address = x42
	signal enable_control_read  : std_logic;
	--! When address = x43
	signal enable_timer_read    : std_logic;

	signal address_select, temp, temp2 : std_logic;
begin
	-- Start Address from x40 to x43 = 0100 0000
	--	enable <= en and addressBus(6) and (not (addressBus(7) or addressBus(5) or addressBus(4) or addressBus(3) or addressBus(2)));

	--	-- bus Read, bus write
	--	enable_control_write <= enable and not rd_wr and (not (regSelect(1) or regSelect(0)));
	--	enable_timer_write   <= enable and not rd_wr and (not regSelect(1)) and regSelect(0);
	--
	--	enable_control_read <= enable and rd_wr and (not (regSelect(1) or regSelect(0)));
	--	enable_timer_read   <= enable and rd_wr and (not regSelect(1)) and regSelect(0);

	enable         <= en and addressEnable;
	--	address_select <= not regSelect(1) and regSelec?t(0);
	address_select <= enable and not regSelect(1);
	lbl_address_decoder : decoder_2x4
		port map(x0 => regSelect(0),
			     x1 => rd_wr,
			     e  => address_select,
			     d0 => enable_control_write,
			     d1 => enable_timer_write,
			     d2 => enable_control_read,
			     d3 => enable_timer_read);

	--	enable_control_write <= enable and rd_wr and (not (addressBus(1) or addressBus(0)));
	--	enable_timer_write   <= enable and rd_wr and (not addressBus(1)) and addressBus(0);
	--	enable_control_read  <= enable and (not rd_wr) and (not addressBus(0)) and addressBus(1);
	--	enable_timer_read    <= enable and (not rd_wr) and addressBus(1) and addressBus(0);

	-- Identify Counter Options
	T0ON   <= en and control_output(0);
	T0CS   <= control_output(1) and not enable_timer_write;
	T0SE   <= control_output(2);
	T0SYNC <= control_output(3) and T0CS;

	-- Select External Clock Edge
	ext_clk_edge_sel <= ext_clk xor T0SE;

	--! Select Clock 
	lbl_clk_mux : mux_2x1 port map(clk, ext_clk_edge_sel, T0CS, clk_select);

	fx_delay1 : TriStateBuffer port map(clk_select, '1', temp);
	fx_delay2 : TriStateBuffer port map(temp, '1', temp2);
	--! Sync External Clock with Internal Clock
	lbl_sync_clk : Sync port map(clk, temp2, sync_clk);

	--! Select Sync, Async
	lbl_final_clk_mux : mux_2x1 port map(clk_select, sync_clk, T0SYNC, final_clk);

	--! Counting Process
	process(final_clk, rst, enable_timer_write, timer_output_reg)
	begin
		if rst = '1' then
			timer_output <= X"00";
			T0IF         <= '0';
		elsif enable_timer_write = '1' then
			timer_output <= timer_output_reg;
		elsif (rising_edge(final_clk)) then
			--			if enable_timer_write = '1' then
			--				timer_output <= timer_output_reg;
			if T0ON = '1' then
				if (timer_output = X"FF") then
					T0IF <= '1';
				else
					T0IF <= '0';
				end if;
				timer_output <= timer_output + '1';
			end if;
		end if;
	end process;

	--! Control Registers
	lbl_register_control : my_nDFF generic map(8) port map(clk, rst, enable_control_write, dataBus, control_output);
	--! Timer/Counter Registers
	lbl_register_timer : my_nDFF generic map(8) port map(clk, rst, enable_timer_write, dataBus, timer_output_reg);

	--! Control Tristates
	lbl_tristate_control : Ntri generic map(8) port map(control_output, enable_control_read, dataBus);
	--! Timer/Counter Tristates
	lbl_tristate_timer : Ntri generic map(8) port map(timer_output, enable_timer_read, dataBus);

end architecture RTL;
