library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
----------------------------------------------------------------
---- PWM module 
---- control register
---- bit 7 ---> enable / disable must set to 1 when working & to 0 when inialize 
----------------------------------------------------------------
entity PWM is
	port(
		clk     : in    std_logic;
		rst     : in    std_logic;
		dataBus : inout std_logic_vector(7 downto 0);
		rd_wr   : in    std_logic;
		en      : in    std_logic;
		Address : in    std_logic;
		S1, S0  : in    std_logic;
		output  : out   std_logic
	);
end entity PWM;

architecture RTL of PWM is
	component my_DFF
		port(d, clk, rst, enable : in  std_logic;
			 q                   : out std_logic);
	end component my_DFF;
	Component my_nDFF is
		Generic(n : integer := 8);
		port(Clk, Rst, enable : in  std_logic;
			 d                : in  std_logic_vector(n - 1 downto 0);
			 q                : out std_logic_vector(n - 1 downto 0));
	end component;
	Component Ntri is
		Generic(n : integer := 8);
		port(
			ins  : in  std_logic_vector(n - 1 downto 0);
			es   : in  std_logic;
			outs : out std_logic_vector(n - 1 downto 0));
	end component;
	-- in this section we define all used signals 
	signal Enable_ICR, ICR_Read, ICR_Write                      : std_logic;
	signal ICR_Output                                           : std_logic_vector(7 downto 0);
	signal Enable_Resolution, Resolution_Read, Resolution_Write : std_logic;
	signal Resolution_Output                                    : std_logic_vector(7 downto 0);
	signal outlatch, outputChange                               : std_logic;
	signal CounterIn, CounterOut                                : std_logic_vector(7 downto 0);
begin
	-----Register read,write signals
	ICR_Read  <= Enable_ICR and rd_wr and en;
	ICR_Write <= Enable_ICR and (not rd_wr) and en;

	Resolution_Read   <= Enable_Resolution and rd_wr and en;
	Resolution_Write  <= Enable_Resolution and (not rd_wr) and en;
	--------------------------------
	Enable_ICR        <= (Address and (not S0) and (not S1));
	Enable_Resolution <= (Address and (S0) and (not S1));
	outlatch          <= ('1' and en) when (CounterIn < ICR_Output)
		else '0' when (CounterOut = ICR_Output);
	outputChange <= '1' when (CounterOut = ICR_Output) or (CounterIn < ICR_Output)
		else '0';
	-- ComponentS Definitions
	----Registers
	lbl_output_latch : component my_DFF
		port map(
			d      => outlatch,
			clk    => clk,
			rst    => rst,
			enable => outputChange,
			q      => output
		);
	lbl_register_ICR : my_nDFF generic map(8) port map(clk, rst, ICR_Write, dataBus, ICR_Output);
	lbl_register_Roslution : my_nDFF generic map(8) port map(clk, rst, Resolution_Write, dataBus, Resolution_Output);
	lbl_register_Counter : my_nDFF generic map(8) port map(clk, rst, '1', CounterIn, CounterOut);
	CounterIn <= "00000000" when CounterOut+1 = Resolution_Output
		else CounterOut + 1;
	----TRistates
	lbl_tristate_ICR : Ntri generic map(8) port map(ICR_Output, ICR_Read, dataBus);
lbl_tristate_Resolution : Ntri generic map(8) port map(Resolution_Output, Resolution_Read, dataBus);
end architecture RTL; 