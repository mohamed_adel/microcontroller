vsim work.main

add wave -position insertpoint  \
sim:/main/clk \
sim:/main/rst \
sim:/main/en \
sim:/main/ext_clk \
sim:/main/rd_wr \
sim:/main/GeneralInterrupt \
sim:/main/addressBus \
sim:/main/dataBus \
sim:/main/IOpins \
sim:/main/Timer0Interrupt \
sim:/main/Timer0Output \
sim:/main/Timer1Interrupt \
sim:/main/Timer1Output \
sim:/main/PWM_Output \
sim:/main/interruptEnableOutput \
GBIO_address \
sim:/main/PWM_address
add wave -position 11  sim:/main/lbl_Timer0/timer_output
add wave -position 12  sim:/main/lbl_Timer0/control_output
add wave -position 15  sim:/main/lbl_Timer1/control_output
add wave -position 16  sim:/main/lbl_Timer1/timer_output
add wave -position 17  sim:/main/lbl_Timer1/period_output
force -freeze sim:/main/clk 1 0, 0 {50 ps} -r 100
force -freeze sim:/main/rst 1 0
run

force -freeze sim:/main/en 1 0
force -freeze sim:/main/rst 0 0
run

force -freeze sim:/main/ext_clk 1 0, 0 {50 ps} -r 100

force -freeze sim:/main/rd_wr 0 0

echo "Enable General Interrupt @ x30, Time: $now"
force -freeze sim:/main/addressBus 16#30 0
force -freeze sim:/main/dataBus 16#03 0
run

echo "Configure GIOPins @ x65, Time: $now"
force -freeze sim:/main/addressBus 16#65 0
force -freeze sim:/main/dataBus 16#07 0
run
run
echo "Configure Timer0 Control Register @ x40, Time: $now"
force -freeze sim:/main/addressBus 16#40 0
force -freeze sim:/main/dataBus 16#3 0
run

echo "Preset Timer0 Timer Register @ x41 to xFA, Time: $now"
force -freeze sim:/main/addressBus 16#41 0
force -freeze sim:/main/dataBus 16#FA 0
run


echo "Configure Timer1 Control Register @ x44, Time: $now"
force -freeze sim:/main/addressBus 16#44 0
force -freeze sim:/main/dataBus 16#01 0
run

echo "Configure Timer1 Period Register @ x46 to x03, Time: $now"
force -freeze sim:/main/addressBus 16#46 0
force -freeze sim:/main/dataBus 16#03 0
run
#force -freeze sim:/main/addressBus 16#0 0
noforce sim:/main/dataBus


echo "Read Timer0 Register @ x41, Time: $now"
force -freeze sim:/main/rd_wr 1 0
force -freeze sim:/main/addressBus 16#41 0
run
run
run
run
run
echo "Read Timer1 Register @ x45, Time: $now"
force -freeze sim:/main/rd_wr 1 0
force -freeze sim:/main/addressBus 16#45 0
run